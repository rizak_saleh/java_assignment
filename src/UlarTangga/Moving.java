package UlarTangga;
import Assignment_Java.*;

public class Moving {
	public int calculatePlayerValue(int playerPosition, int diceValue) {
		int playerNewPosition=playerPosition+diceValue;

		if (playerNewPosition > WIN)
			return playerPosition;		

		if (null !=snake.get(playerNewPosition)) {
			System.out.println("Oops..swallowed by the snake..");
			playerNewPosition=snake.get(playerNewPosition);
		}
		
		if (null !=ladder.get(playerNewPosition)) {
			System.out.println("YAY! climbing the ladder..");
			playerNewPosition=ladder.get(playerNewPosition);
		}
		
		return playerNewPosition;
	}
}
